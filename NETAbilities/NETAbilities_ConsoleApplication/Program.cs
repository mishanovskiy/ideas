﻿using NETAbilities_Core;

namespace NETAbilities_ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Repository<Film> repository = new Repository<Film>();
            repository.AddRange(RepositoryUtility<Film>.GetDataFromFile("films.xml"));
            UserInterface.RepositoryHandler(repository);   
        }
    }
}
