﻿using System;
using NETAbilities_Core;

namespace NETAbilities_ConsoleApplication
{
    // Пользовательский интерфейс консольного приложения
    static class UserInterface
    {
        // Вывод на консоль содержимого хранилища
        public static void PrintRepository(Repository<Film> repository)
        {
            repository.GetRepository().ForEach((film) =>
            {
                Console.WriteLine(film);
            });
            Console.ReadKey();
        }
        // Обработчик репозитория(меню)
        public static void RepositoryHandler(Repository<Film> repository)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Menu: ");
                Console.WriteLine("1. Add to repository\n"
                    + "2. Remove from repository\n"
                    + "3. Print repository\n"
                    + "4. Exit\n");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        {
                            repository.Add(GetFilm());
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter filmname to remove: ");
                            string filmName = Console.ReadLine();
                            repository.Remove(Utility.SearchFilmByName(repository, filmName) ?? new Film());
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Films in storage: ");
                            PrintRepository(repository);
                            break;
                        }
                    case 4:
                        {
                            RepositoryUtility<Film>.SaveDataToFile(repository.GetRepository(), "films.xml");
                            Environment.Exit(1);
                            break;
                        }
                }
            }
        }
        /// <summary>
        /// Заполняет информацию о фильме
        /// </summary>
        /// <returns>фильм с заполненой информацией</returns>
        public static Film GetFilm()
        {
            Film film = new Film();
            Console.WriteLine("Enter film name: ");
            film.Name = Console.ReadLine();
            Console.WriteLine("Enter film description: ");
            film.Description = Console.ReadLine();
            Console.WriteLine("Enter film rating: ");
            film.Rating = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter link to film(http:// format): ");
            film.LinkToWatch = Console.ReadLine();
            return film;
        }
    }
}
