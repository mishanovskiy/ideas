﻿using NETAbilities_Core;

namespace NETAbilities_ConsoleApplication
{
    // Утилитарный класс для текущей версии приложения
    static class Utility
    {
        // Поиск фильм по названию
        public static Film SearchFilmByName(Repository<Film> repository, string filmName)
        {
            return repository.GetRepository().Find(film => film.Name == filmName);
        }
    }
}
