﻿namespace NETAbilities_Core
{
    // Класс для описания сущности "Фильм"
    public class Film
    {
        // Название фильма
        public string Name { get; set; } 
        // Рейтинг фильма
        public int Rating { get; set; }
        // Описание фильма
        public string Description { get; set; }
        // Ссілка для просмотра(скачивания)
        public string LinkToWatch { get; set; }
        // Информация о фильме
        public override string ToString()
        {
            return string.Format("{0}{4}{1}{4}{2}{4}{3}{4}", Name, Description,
                Rating, LinkToWatch, System.Environment.NewLine);
        }
    }
}
