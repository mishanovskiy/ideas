﻿using System.Collections.Generic;

namespace NETAbilities_Core
{
    // Типизированое хранилище данных
    public class Repository<T>
    {
        // Список для хранения элементов
        private List<T> _data = new List<T>();
        /// <summary>
        /// Получить список элементов
        /// </summary>
        /// <returns>Список элементов</returns>
        public List<T> GetRepository()
        {
            return _data;
        }
        // Добавить элемент в хранилище
        public void Add(T item)
        {
            _data.Add(item);
        }
        /// <summary>
        /// Удалить элемент из хранилища
        /// </summary>
        /// <param name="item">Элемент для удаления</param>
        /// <returns>Удалилось ли(если фолс, то переданный эл-т не существует)</returns>
        public bool Remove(T item)
        {
            return _data.Remove(item);
        }
        /// <summary>
        /// Добавление множества єлементов в хранилище
        /// </summary>
        /// <param name="enumerable">Множество єлементов</param>
        public void AddRange(IEnumerable<T> enumerable)
        {
            _data.AddRange(enumerable);
        }

        // Количество элементов хранилища
        public int Count
        {
            get
            {
                return _data.Count;
            }
        }
    }
}
