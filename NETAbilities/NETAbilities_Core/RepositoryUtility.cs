﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace NETAbilities_Core
{
    // Утилитарный класс для хранилища
    public static class RepositoryUtility<T>
    {
        // Сериализатор, рутовая нода пусть будет стандартная
        private static XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<T>));
        /// <summary>
        /// Сохранение информации из хранилища в xml файл
        /// </summary>
        /// <param name="repository">Хранилище</param>
        /// <param name="filename">Имя файла для сохранения</param>
        public static void SaveDataToFile(List<T> repository, string filename)
        {
            using (var fs = new FileStream(@"..\..\" + filename , FileMode.Create))
            {
                xmlSerializer.Serialize(fs, repository);
            }
        }
        /// <summary>
        /// Получение информации в хранилище из xml файла
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns>Хранилище с полученной информацией</returns>
        public static List<T> GetDataFromFile(string filename)
        {
            List<T> items;
            using (var fs = new FileStream(@"..\..\" + filename, FileMode.Open))
            {
                items = (List<T>)xmlSerializer.Deserialize(fs);
            }
            return items;
        }
    }
}
