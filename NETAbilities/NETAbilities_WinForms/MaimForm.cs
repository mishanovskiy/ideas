﻿using NETAbilities_Core;
using System.Windows.Forms;

namespace NETAbilities_WinForms
{
    public partial class MaimForm : Form
    {
        // Хранилище информации о фильмах.
        Repository<Film> _repository = new Repository<Film>();
        
        public MaimForm()
        {
            InitializeComponent();
            _repository.AddRange(RepositoryUtility<Film>.GetDataFromFile("films.xml"));
            UpdateListView();
        }

        // Обрабатывает нажатие кнопки Remove - удаление выбранного фильма.
        private void buttonRemove_Click(object sender, System.EventArgs e)
        {
            if (listViewMain.Items.Count == 0)
            {
                return;
            }
            string selectedText = listViewMain.SelectedItems[0].Text;
            int index = selectedText.IndexOf(":");
            selectedText = selectedText.Substring(0, index);
            Film film = _repository.GetRepository().Find(file => file.Name.Equals(selectedText));
            _repository.Remove(film);
            UpdateListView();            
        }

        // Обрабатывает нажатие кнопки Add - добавление нового фильма в список.
        private void buttonAdd_Click(object sender, System.EventArgs e)
        {
            Film film = new Film();
            film.Name = textBoxName.Text;
            film.Description = textBoxDescription.Text;
            film.Rating = trackBarRaiting.Value;
            film.LinkToWatch = textBoxLink.Text;
            _repository.Add(film);
            UpdateListView();
        }

        // Обрабатывает прокрутку элемента TrackBar - отображает подсказку с рейтингом.
        private void trackBarRaiting_Scroll(object sender, System.EventArgs e)
        {
            toolTipMain.SetToolTip(trackBarRaiting, trackBarRaiting.Value.ToString());
    }

        // Очищает и с нуля заполняет ListView.
        private void UpdateListView()
        {
            listViewMain.Items.Clear();
            foreach (var item in _repository.GetRepository())
            {
                listViewMain.Items.Add(item.Name + ": " + item.Rating + " баллов из 10");
            }
        }
    }
}
